module modernc.org/rec

go 1.20

require (
	github.com/dustin/go-humanize v1.0.1
	modernc.org/gc/v3 v3.0.0-20230512134359-466b49aa80e0
	modernc.org/regexp v1.6.0
)

require (
	github.com/hashicorp/golang-lru/v2 v2.0.1 // indirect
	github.com/remyoudompheng/bigfft v0.0.0-20200410134404-eec4a21b6bb0 // indirect
	modernc.org/fsm v1.2.0 // indirect
	modernc.org/mathutil v1.5.0 // indirect
	modernc.org/sortutil v1.1.1 // indirect
	modernc.org/strutil v1.1.3 // indirect
	modernc.org/token v1.1.0 // indirect
)
