# Copyright 2023 The Rec Authors. All rights reserved.
# Use of this source code is governed by a BSD-style
# license that can be found in the LICENSE file.

.PHONY:	all bench clean edit editor generate test

all:
	golint 2>&1 | tee -a log-editor
	staticcheck 2>&1 | tee -a log-editor

bench:
	date > log-bench
	go test -bench . 2>&1 | tee -a log-bench

clean:
	rm -f *.text *.out *.test
	go clean

edit:
	@touch log
	@if [ -f "Session.vim" ]; then gvim -S & else gvim -p Makefile *.go & fi

editor:
	date > log-editor
	gofmt -l -s -w *.go
	go install -v
	go test -c -o /dev/null 2>&1 | tee -a log-editor
	golint 2>&1 | tee -a log-editor
	date >> log-editor

generate:
	# go install modernc.org/rec@latest
	rec -match=testMatch -pkg main -import=bytes,strings,unicode/utf8 'sshd\[\d{5}\]:\s*Failed' > match_test.go
	rec -match=testMatch2 'sshd\[\d{5}\]:\s*Failed\z' >> match_test.go
	rec -match=testMatch3 '\Asshd\[\d{5}\]:\s*Failed' >> match_test.go
	rec -match=testMatch4 '\Asshd\[\d{5}\]:\s*Failed\z' >> match_test.go
	
	rec -matchstring=testMatchString 'sshd\[\d{5}\]:\s*Failed' >> match_test.go
	rec -matchstring=testMatchString2 'sshd\[\d{5}\]:\s*Failed\z' >> match_test.go
	rec -matchstring=testMatchString3 '\Asshd\[\d{5}\]:\s*Failed' >> match_test.go
	rec -matchstring=testMatchString4 '\Asshd\[\d{5}\]:\s*Failed\z' >> match_test.go
	
	rec -matchutf8=testMatchUtf8 'sshd\[\d{5}\]:\s*Failed' >> match_test.go
	rec -matchutf8=testMatch2Utf8 'sshd\[\d{5}\]:\s*Failed\z' >> match_test.go
	rec -matchutf8=testMatch3Utf8 '\Asshd\[\d{5}\]:\s*Failed' >> match_test.go
	rec -matchutf8=testMatch4Utf8 '\Asshd\[\d{5}\]:\s*Failed\z' >> match_test.go
	
	rec -matchstringutf8=testMatchStringUtf8 'sshd\[\d{5}\]:\s*Failed' >> match_test.go
	rec -matchstringutf8=testMatch2StringUtf8 'sshd\[\d{5}\]:\s*Failed\z' >> match_test.go
	rec -matchstringutf8=testMatch3StringUtf8 '\Asshd\[\d{5}\]:\s*Failed' >> match_test.go
	rec -matchstringutf8=testMatch4StringUtf8 '\Asshd\[\d{5}\]:\s*Failed\z' >> match_test.go

	rec -lex=testLex -pkg main -import=unicode,unicode/utf8 '[a-z]+' '[0-9]+' > lex_test.go
	rec -lexstring=testLexString '[a-z]+' '[0-9]+' >> lex_test.go
	rec -lexutf8=testLexUtf8 '\pL+' '\p{Nd}+' >> lex_test.go
	rec -lexstringutf8=testLexStringUtf8 '\pL+' '\p{Nd}+' >> lex_test.go

	./scanner.sh	

	gofmt -l -s -w .

test:
	date > log-test
	go test 2>&1 | tee -a log-test
