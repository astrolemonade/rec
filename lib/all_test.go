// Copyright 2023 The Rec Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package rec // modernc.org/rec/lib

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"testing"
	"time"
	"unicode"

	"modernc.org/regexp"
)

func TestMain(m *testing.M) {
	rc := m.Run()
	os.Exit(rc)
}

// origin returns caller's short position, skipping skip frames.
func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
		if strings.HasPrefix(fns, "func") {
			num := true
			for _, c := range fns[len("func"):] {
				if c < '0' || c > '9' {
					num = false
					break
				}
			}
			if num {
				return origin(skip + 2)
			}
		}
	}
	return fmt.Sprintf("%s:%d:%s", filepath.Base(fn), fl, fns)
}

// todo prints and returns caller's position and an optional message tagged with TODO. Output goes to stderr.
//
//lint:ignore U1000 whatever
func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	s = fmt.Sprintf("%s\n\tTODO %s", origin(2), s)
	// fmt.Fprintf(os.Stderr, "%s\n", s)
	// os.Stdout.Sync()
	return s
}

// trc prints and returns caller's position and an optional message tagged with TRC. Output goes to stderr.
//
//lint:ignore U1000 whatever
func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	s = fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stderr, "%s\n", s)
	os.Stderr.Sync()
	return s
}

//lint:ignore U1000 whatever
func stack() []byte { return debug.Stack() }

//lint:ignore U1000 whatever
func line() int {
	_, _, line, _ := runtime.Caller(1)
	return line
}

func str(s string) string {
	return strconv.QuoteToASCII(s)
}

func TestCorpus(t *testing.T) {
	m, err := filepath.Glob("corpus*")
	if err != nil {
		t.Skip(err)
	}

	t0 := time.Now()
	dir := t.TempDir()
	wd, err := os.Getwd()
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		if err := os.Chdir(wd); err != nil {
			t.Fatal(err)
		}
	}()

	if err := os.Chdir(dir); err != nil {
		t.Fatal(err)
	}

	for _, fn := range m {
		if time.Since(t0) > 8*time.Hour {
			return
		}

		t.Run(fn, func(t *testing.T) { testCorpus(t, dir, filepath.Join(wd, fn)) })
	}
}

func testCorpus(t *testing.T, dir, cfn string) {
	f, err := os.Open(cfn)
	if err != nil {
		t.Fatalf("%s: %v", cfn, err)
	}

	defer f.Close()

	const maxLine = 1 << 16
	s := bufio.NewScanner(f)
	s.Buffer(make([]byte, maxLine), maxLine)
	batch := &batch{
		dir: dir,
		t:   t,
	}
	var item batchItem
	var lineno int
	for s.Scan() {
		line := s.Text()
		line0 := line
		if line == "" {
			t.Fatal("unexpected empty line")
		}

		p := line[0]
		line, err := strconv.Unquote(line[1:])
		if err != nil {
			t.Fatalf("%q: %v", line0, err)
		}

		lineno++
		switch p {
		case 'e':
			batch.add(item)
			item = batchItem{re: line, reLine: lineno}
		case 'i':
			item.ins = append(item.ins, line)
		default:
			t.Fatalf("%s: invalid line %q", cfn, line0)
		}
	}
	batch.add(item)
	batch.flush()
	if err := s.Err(); err != nil {
		t.Fatalf("%s: %v", cfn, err)
	}
	t.Logf("%s: lines %v", cfn, lineno)
}

type batchItem struct {
	re     string
	reLine int
	ins    []string
}

func (bi *batchItem) render(b *batch) (r []string) {
	engineName := fmt.Sprintf("match%d", bi.reLine)
	rc, err := Main(
		[]string{
			"-matchstringutf8", engineName,
			bi.re,
		},
		b.wr, io.Discard,
	)
	if err != nil {
		return nil
	}

	if rc != 0 {
		panic(todo("rc %d", rc))
	}

	re, err := regexp.Compile(bi.re)
	if err != nil {
		b.t.Fatalf("Compile(`%s`): %v", pretty(bi.re), err)
	}

	testName := fmt.Sprintf("test%d", bi.reLine)
	b.w(`
 func %s() {
 `, testName,
	)

	defer func() { b.w("}\n\n") }()

	r = append(r, testName)
	var ins []string
	in := bi.ins
	if len(in) == 0 {
		in = append(in, string(rune(bi.reLine%unicode.MaxRune)), "")
	}
	for _, in := range in {
		ins = ins[:0]
		for l := 0; l < 4 && l < len(in); l++ {
			for r := len(in); r > len(in)-4 && l < r; r-- {
				ins = append(ins, in[l:r])
			}
		}
		for i, in := range ins {
			e := re.MatchString(in)
			s := ""
			if e {
				s = "!"
			}
			b.w("\tif in := %s; %s%s(in) { fail(%q, %s, in, %strue) }\n", str(in), s, engineName, fmt.Sprintf("%s.%d", engineName, i), str(bi.re), s)
		}
	}
	return r
}

type batch struct {
	dir     string
	items   []batchItem
	modOnce sync.Once
	n       int
	t       *testing.T
	wr      *bufio.Writer
}

func (b *batch) w(s string, args ...interface{}) {
	if _, err := fmt.Fprintf(b.wr, s, args...); err != nil {
		b.t.Fatal(err)
	}
}

func (b *batch) add(item batchItem) {
	const maxBatch = 100
	if item.re == "" || len(item.ins) == 0 {
		return
	}

	b.items = append(b.items, item)
	if len(b.items) == maxBatch {
		b.flush()
	}
}

func (b *batch) flush() {
	if len(b.items) == 0 {
		return
	}

	defer func() { b.items = b.items[:0] }()

	b.n++
	fn := fmt.Sprintf("main%d.go", b.n)

	defer os.Remove(fn)

	if b.render(fn) {
		b.exec(fn, b.items[0].reLine)
	}
}

func (b *batch) render(fn string) bool {
	f, err := os.Create(fn)
	if err != nil {
		b.t.Fatal(err)
	}

	defer func() {
		if err := f.Sync(); err != nil {
			b.t.Fatal(err)
		}

		if err := f.Close(); err != nil {
			b.t.Fatal(err)
		}
	}()

	b.wr = bufio.NewWriter(f)

	defer func() {
		if err := b.wr.Flush(); err != nil {
			b.t.Fatal(err)
		}
	}()

	b.w(`package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode/utf8"
)

`)

	var tests []string
	for _, item := range b.items {
		tests = append(tests, item.render(b)...)
	}
	if len(tests) == 0 {
		return false
	}

	b.w("%s", `
func fail(tests, re, s string, exp bool) { fmt.Printf("%s: re `+"`"+`%s`+"`"+` input %s got %v exp %v\n", tests, str(re), str(s), exp, !exp); os.Exit(1) }

func pretty(s string) string { return strings.Join(strings.Split(s, "\n"), "\\n") }

func str(s string) string { return strconv.QuoteToASCII(s) }

func main() {
`)
	for _, v := range tests {
		b.w("\t%s()\n", v)
	}
	b.w("}\n")
	return true
}

func (b *batch) exec(fn string, reLine int) {
	// bb, err := os.ReadFile(fn)
	// trc("==== %v\n%s\n----", err, bb)
	b.modOnce.Do(func() {
		if err := exec.Command("go", "mod", "init", "example.com/rec").Run(); err != nil {
			b.t.Fatal(err)
		}
	})

	b.t.Log(reLine, time.Now().Format("2006-01-02 15:04:05"))
	if out, err := exec.Command("go", "run", fn).CombinedOutput(); err != nil {
		b.t.Fatalf("%s\nFAIL: err %v", out, err)
	}
}
